import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-content',
  templateUrl: './card-content.component.html',
  styleUrls: ['./card-content.component.css']
})
export class CardContentComponent implements OnInit {
  contents = [];

  constructor() { }

  ngOnInit() {
    this.createContents();
  }

  createContents(){
    for(let index = 0; index < 3; index++){
      this.contents.push(index +1);
    }
  }

}
